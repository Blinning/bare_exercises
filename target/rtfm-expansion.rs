# [allow (non_snake_case)] fn init (cx : init :: Context) -> init ::
LateResources
{
    let mut syst = cx . core . SYST ; let device = cx . device ; syst .
    set_clock_source (SystClkSource :: Core) ; syst . set_reload (16_000_000)
    ; syst . enable_counter () ; syst . enable_interrupt () ; device . RCC .
    ahb1enr . modify (| _, w | w . gpioaen () . set_bit ()) ; device . GPIOA .
    moder . modify (| _, w | w . moder5 () . bits (1)) ; init :: LateResources
    { GPIOA : device . GPIOA, }
} # [allow (non_snake_case)] fn toggle
(toggle :: Locals { TOGGLE, .. } : toggle :: Locals, cx : toggle :: Context)
{
    use rtfm :: Mutex as _ ; hprintln ! ("toggle {:?}", TOGGLE) . unwrap () ;
    if * TOGGLE
    {
        cx . resources . GPIOA . bsrr . write (| w | w . bs5 () . set_bit ())
        ;
    } else
    {
        cx . resources . GPIOA . bsrr . write (| w | w . br5 () . set_bit ())
        ;
    } * TOGGLE = ! * TOGGLE ;
} # [doc = r" Resources initialized at runtime"] # [allow (non_snake_case)]
pub struct initLateResources { pub GPIOA : stm32 :: GPIOA } #
[allow (non_snake_case)] # [doc = "Initialization function"] pub mod init
{
    # [doc (inline)] pub use super :: initLateResources as LateResources ; #
    [doc = r" Execution context"] pub struct Context < >
    {
        # [doc = r" Core (Cortex-M) peripherals"] pub core : rtfm :: export ::
        Peripherals, # [doc = r" Device peripherals"] pub device :
        stm32f4xx_hal :: stm32 :: Peripherals,
    } impl < > Context < >
    {
        # [inline (always)] pub unsafe fn new
        (core : rtfm :: export :: Peripherals,) -> Self
        {
            Context
            {
                device : stm32f4xx_hal :: stm32 :: Peripherals :: steal (),
                core,
            }
        }
    }
} # [allow (non_snake_case)] # [doc = "Resources `toggle` has access to"] pub
struct toggleResources < 'a > { pub GPIOA : & 'a mut stm32 :: GPIOA, } #
[allow (non_snake_case)] # [doc = "Hardware task"] pub mod toggle
{
    # [doc (inline)] pub use super :: toggleLocals as Locals ; #
    [doc (inline)] pub use super :: toggleResources as Resources ; #
    [doc = r" Execution context"] pub struct Context < 'a >
    {
        # [doc = r" Resources this task has access to"] pub resources :
        Resources < 'a >,
    } impl < 'a > Context < 'a >
    {
        # [inline (always)] pub unsafe fn new
        (priority : & 'a rtfm :: export :: Priority) -> Self
        { Context { resources : Resources :: new (priority), } }
    }
} # [allow (non_snake_case)] # [doc (hidden)] pub struct toggleLocals < 'a >
{ TOGGLE : & 'a mut bool } impl < 'a > toggleLocals < 'a >
{
    # [inline (always)] unsafe fn new () -> Self
    {
        static mut TOGGLE : bool = false ; toggleLocals
        { TOGGLE : & mut TOGGLE }
    }
} # [doc = r" Implementation details"] const APP : () =
{
    #
    [doc =
     r" Always include the device crate which contains the vector table"] use
    stm32f4xx_hal :: stm32 as _ ; # [cfg (core = "1")] compile_error !
    ("specified 1 core but tried to compile for more than 1 core") ; #
    [allow (non_upper_case_globals)] # [link_section = ".uninit.rtfm0"] static
    mut GPIOA : core :: mem :: MaybeUninit < stm32 :: GPIOA > = core :: mem ::
    MaybeUninit :: uninit () ; # [allow (non_snake_case)] # [no_mangle] unsafe
    fn SysTick ()
    {
        const PRIORITY : u8 = 1u8 ; rtfm :: export :: run
        (PRIORITY, ||
         {
             crate :: toggle
             (toggle :: Locals :: new (), toggle :: Context :: new
              (& rtfm :: export :: Priority :: new (PRIORITY)))
         }) ;
    } impl < 'a > toggleResources < 'a >
    {
        # [inline (always)] unsafe fn new
        (priority : & 'a rtfm :: export :: Priority) -> Self
        { toggleResources { GPIOA : & mut * GPIOA . as_mut_ptr (), } }
    } # [no_mangle] unsafe extern "C" fn main () -> !
    {
        rtfm :: export :: assert_send :: < stm32 :: GPIOA > () ; rtfm ::
        export :: interrupt :: disable () ; let mut core : rtfm :: export ::
        Peripherals = core :: mem :: transmute (()) ; let _ =
        [() ;
         ((1 << stm32f4xx_hal :: stm32 :: NVIC_PRIO_BITS) - 1u8 as usize)] ;
        core . SCB . set_priority
        (rtfm :: export :: SystemHandler :: SysTick, rtfm :: export ::
         logical2hw (1u8, stm32f4xx_hal :: stm32 :: NVIC_PRIO_BITS),) ; core .
        SCB . scr . modify (| r | r | 1 << 1) ; let late = init
        (init :: Context :: new (core . into ())) ; GPIOA . as_mut_ptr () .
        write (late . GPIOA) ; rtfm :: export :: interrupt :: enable () ; loop
        { rtfm :: export :: wfi () }
    }
} ;